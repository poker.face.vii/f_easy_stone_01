import 'package:mobx/mobx.dart';

part 'navbarstate.g.dart';

class BottomNavState = _BottomNavState with _$BottomNavState;

abstract class _BottomNavState with Store {
  @observable
  List nav = [1, 0, 0, 0];
  double count = 1;

  @action
  void change1() {
    nav = [0, 0, 0, 0];
    nav[0] == 0 ? nav[0] = 1 : nav[0] = 0;
    count++;
  }

  @action
  void change2() {
    nav = [0, 0, 0, 0];
    nav[1] == 0 ? nav[1] = 1 : nav[1] = 0;
  }

  @action
  void change3() {
    nav = [0, 0, 0, 0];
    nav[2] == 0 ? nav[2] = 1 : nav[2] = 0;
  }

  @action
  void change4() {
    nav = [0, 0, 0, 0];
    nav[3] == 0 ? nav[3] = 1 : nav[3] = 0;
  }
}
