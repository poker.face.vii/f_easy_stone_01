// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'navbarstate.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BottomNavState on _BottomNavState, Store {
  final _$navAtom = Atom(name: '_BottomNavState.nav');

  @override
  List<dynamic> get nav {
    _$navAtom.context.enforceReadPolicy(_$navAtom);
    _$navAtom.reportObserved();
    return super.nav;
  }

  @override
  set nav(List<dynamic> value) {
    _$navAtom.context.conditionallyRunInAction(() {
      super.nav = value;
      _$navAtom.reportChanged();
    }, _$navAtom, name: '${_$navAtom.name}_set');
  }

  final _$_BottomNavStateActionController =
      ActionController(name: '_BottomNavState');

  @override
  void change1() {
    final _$actionInfo = _$_BottomNavStateActionController.startAction();
    try {
      return super.change1();
    } finally {
      _$_BottomNavStateActionController.endAction(_$actionInfo);
    }
  }

  @override
  void change2() {
    final _$actionInfo = _$_BottomNavStateActionController.startAction();
    try {
      return super.change2();
    } finally {
      _$_BottomNavStateActionController.endAction(_$actionInfo);
    }
  }

  @override
  void change3() {
    final _$actionInfo = _$_BottomNavStateActionController.startAction();
    try {
      return super.change3();
    } finally {
      _$_BottomNavStateActionController.endAction(_$actionInfo);
    }
  }

  @override
  void change4() {
    final _$actionInfo = _$_BottomNavStateActionController.startAction();
    try {
      return super.change4();
    } finally {
      _$_BottomNavStateActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = 'nav: ${nav.toString()}';
    return '{$string}';
  }
}
