// ?     Dependency  --------------------------
import 'package:flutter/material.dart';
// .
// ?     SCREENS  ----------------------------------
import 'package:easy_stone_01/screens/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Easy Ston',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Color(0xff2e2e2e),
          accentColor: Color(0xfffeca76),
          scaffoldBackgroundColor: Color(0xfff5f5f5),
          textTheme: Theme.of(context).textTheme.copyWith(
                headline: Theme.of(context).textTheme.headline.apply(
                      fontSizeDelta: 5,
                      fontWeightDelta: 10,
                      color: Colors.black,
                      fontFamily: 'Bellota',
                    ),
                body1: Theme.of(context).textTheme.body1.apply(
                    fontFamily: 'Bellota',
                    fontWeightDelta: 100,
                    fontSizeDelta: 12),
                body2: Theme.of(context).textTheme.body2.apply(
                    fontFamily: 'Bellota',
                    fontWeightDelta: 2,
                    fontSizeDelta: 8),
              )),
      initialRoute: '/',
      routes: {
        '/': (context) => HomeScreen(),
        // '/complited': (context) => ComplitedPage(),
        // '/': (context) => TestPage(),
        // '/other': (context) => InboxPage(),
      },
    );
  }
}
