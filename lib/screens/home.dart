// ?     Dependency  --------------------------
import 'package:flutter/material.dart';

// ?     Widgets  --------------------------
import 'package:easy_stone_01/widget/Group_item.dart';
import 'package:easy_stone_01/widget/LastViewed_item.dart';
import 'package:easy_stone_01/widget/Animated_navigation_bar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(children: <Widget>[
          Positioned(
            top: -35,
            left: 0,
            right: 0,
            child: Container(
              margin: EdgeInsets.only(left: 70, right: 70),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                // color: Colors.amber[200],
              ),
              height: 90,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              top: 10,
            ),
            child: ListView(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      "assets/image/icone.png",
                      height: 40,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Easy Stone",
                      style: Theme.of(context).textTheme.headline,
                    )
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  "Search",
                  style: Theme.of(context).textTheme.body1,
                ),
                Text(
                  "for minerals",
                  style: Theme.of(context).textTheme.body2,
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(14),
                        ),
                        child: TextField(
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                top: 19,
                                bottom: 19,
                              ),
                              icon: Padding(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Icon(Icons.search)),
                              hintText: "Search"),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      child: Container(
                        width: 58,
                        height: 58,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Theme.of(context).accentColor),
                        child: Icon(Icons.tune),
                      ),
                      onTap: () {},
                    )
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Text("Groups"),
                SizedBox(
                  height: 10,
                ),
                _group(),
                SizedBox(
                  height: 20,
                ),
                Text("Last Viewed"),
                SizedBox(
                  height: 15,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: <Widget>[
                      LastViewedItem(
                        image: "assets/image/g1.png",
                        title: ' Black coil',
                        price: "200.00",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      LastViewedItem(
                        image: "assets/image/g2.png",
                        title: ' Black coil',
                        price: "450.00",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      LastViewedItem(
                        image: "assets/image/g3.png",
                        title: ' Black coil',
                        price: "99.00",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      LastViewedItem(
                        image: "assets/image/g4.png",
                        title: ' Black coil',
                        price: "2.35",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      LastViewedItem(
                        image: "assets/image/g5.png",
                        title: ' Black coil',
                        price: "10.00",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      LastViewedItem(
                        image: "assets/image/g6.png",
                        title: ' Black coil',
                        price: "250.00",
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ]),
      ),
      bottomNavigationBar: AnimatedNavBar(),
    );
  }

// --------------------------------------------------------------
//  !     This is Group Scroll Row
// --------------------------------------------------------------
  Widget _group() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: <Widget>[
          GroupItem(
            icon: "assets/image/g1.png",
            title: "Mineral",
          ),
          SizedBox(
            width: 10,
          ),
          GroupItem(
            icon: "assets/image/g2.png",
            title: "Mineral",
          ),
          SizedBox(
            width: 10,
          ),
          GroupItem(
            icon: "assets/image/g3.png",
            title: "Mineral",
          ),
          SizedBox(
            width: 10,
          ),
          GroupItem(
            icon: "assets/image/g4.png",
            title: "Mineral",
          ),
          SizedBox(
            width: 10,
          ),
          GroupItem(
            icon: "assets/image/g5.png",
            title: "Mineral",
          ),
          SizedBox(
            width: 10,
          ),
          GroupItem(
            icon: "assets/image/g6.png",
            title: "Mineral",
          ),
        ],
      ),
    );
  }
}
 Widget ccc(){
   return Container(child: Text('data'),);
 }