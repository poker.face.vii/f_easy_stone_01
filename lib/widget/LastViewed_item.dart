import 'package:flutter/material.dart';

class LastViewedItem extends StatelessWidget {
  final String image;
  final String title;
  final String price;

  const LastViewedItem({this.image, this.title, this.price});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 260,
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.blueGrey[900]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Image.asset(
            image,
            width: 120,
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            title,
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "\$  " + price,
            style: TextStyle(fontSize: 22, color: Colors.amber),
          )
        ],
      ),
    );
  }
}
