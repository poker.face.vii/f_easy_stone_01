import 'package:easy_stone_01/mobX/navbarstate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

class AnimatedNavBar extends StatefulWidget {
  @override
  _AnimatedNavBarState createState() => _AnimatedNavBarState();
}

class _AnimatedNavBarState extends State<AnimatedNavBar> {
  final BottomNavState navState = BottomNavState();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 22, top: 10),
      child: Observer(
        builder: (_) => Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            InkWell(
              onTap: () {
                setState(() {
                  navState.change1();
                });
              },
              child: AnimatedContainer(
                  duration: Duration(milliseconds: 200),
                  curve: Curves.fastOutSlowIn,
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: navState.nav[0] == 1
                          ? Colors.blueGrey[900]
                          : Color(0xfff5f5f5),
                      borderRadius: BorderRadiusDirectional.circular(12)),
                  child: Icon(
                    Icons.home,
                    color: navState.nav[0] == 1
                        ? Colors.white
                        : Colors.blueGrey[900],
                  )),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  navState.change2();
                });
              },
              child: AnimatedContainer(
                  duration: Duration(milliseconds: 400),
                  curve: Curves.fastOutSlowIn,
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: navState.nav[1] == 1
                          ? Colors.blueGrey[900]
                          : Color(0xfff5f5f5),
                      borderRadius: BorderRadiusDirectional.circular(12)),
                  child: Icon(
                    Icons.insert_chart,
                    color: navState.nav[1] == 1
                        ? Colors.white
                        : Colors.blueGrey[900],
                  )),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  navState.change3();
                });
              },
              child: AnimatedContainer(
                  duration: Duration(milliseconds: 400),
                  curve: Curves.fastOutSlowIn,
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: navState.nav[2] == 1
                          ? Colors.blueGrey[900]
                          : Color(0xfff5f5f5),
                      borderRadius: BorderRadiusDirectional.circular(12)),
                  child: Icon(
                    Icons.location_on,
                    color: navState.nav[2] == 1
                        ? Colors.white
                        : Colors.blueGrey[900],
                  )),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  navState.change4();
                });
              },
              child: AnimatedContainer(
                  duration: Duration(milliseconds: 400),
                  curve: Curves.fastOutSlowIn,
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: navState.nav[3] == 1
                          ? Colors.blueGrey[900]
                          : Color(0xfff5f5f5),
                      borderRadius: BorderRadiusDirectional.circular(12)),
                  child: Icon(
                    Icons.show_chart,
                    color: navState.nav[3] == 1
                        ? Colors.white
                        : Colors.blueGrey[900],
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
