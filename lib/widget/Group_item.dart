import 'package:flutter/material.dart';

class GroupItem extends StatelessWidget {
  final String icon ;
  final String title;

  const GroupItem( {this.icon, this.title});
  
  @override
  Widget build(BuildContext context) {
    return Container(
                        padding: EdgeInsets.all(6),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadiusDirectional.circular(13)),
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(3),
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(13),
                                color: Colors.blueGrey[200],
                              ),
                              child: Image.asset(icon),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              title,
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      );
  }
}